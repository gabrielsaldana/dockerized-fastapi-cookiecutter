#!/usr/bin/env bash
set -euo pipefail

exec python ${APP_PATH}/source/main.py
