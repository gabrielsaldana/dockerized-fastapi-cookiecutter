import os
from pydantic import BaseSettings


class Settings(BaseSettings):
    app_name: str = {{cookiecutter.project_name}}
    admin_email: str = {{cookiecutter.author_email}}

    class Config:
        env_file = os.path.join(os.getenv("APP_PATH"), "local.env")
