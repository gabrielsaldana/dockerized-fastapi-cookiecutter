import uvicorn
from functools import lru_cache
from fastapi import FastAPI

from .config import Depends, Settings

app = FastAPI()


@lru_cache()
def get_settings():
    return Settings()


@app.get("/")
async def root(settings: config.Settings = Depends(get_settings)):
    a = "a"
    b = "b" + a
    return {"hello world": b}


if __name__ == "__main__":
    uvicorn.run("main:app", host="0.0.0.0", port=8000, reload=True)
