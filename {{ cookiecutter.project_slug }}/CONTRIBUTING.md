# Contributing to {{ cookiecutter.project_name }}

We recommend [installing pipx](https://github.com/pypa/pipx) to manage system-wide dependencies.

Using pipx, install [pre-commit](https://pre-commit.com/) globally.

Navigate to the root of the project, then run:

``` shell
pre-commit install
```

This will setup the git hooks to check your code according to project's standards before submitting to the repository.

The first time you try to commit changes, pre-commit will setup and install the needed packages and dependencies to check and lint your code, so it might take several minutes to finish. Once that is set up the first time, subsequent commits won't take as long.

## Shortcuts

The project's `Makefile` includes several shortcuts to run tasks locally. Use `make help` to get a description of the available shortcuts.

## Guidelines

- Add a descriptive comment to *all* methods.
- All work submitted should include its unit tests.
- If the PR submitted involves a new endpoint or update to an endpoint, include an example of the request URL, request payload and response.
- If the PR submitted invovles a UI change, include a screenshot or animated GIF showing the change. We recommend using [liceap](https://www.cockos.com/licecap/) to screen capture in GIF format.

## Git branching strategy

Define here your git branching strategy
